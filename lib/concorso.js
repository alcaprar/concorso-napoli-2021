import { SUPPORTED_NAMES } from './constant'
export default class Concorso {

  constructor (name) {
    if (!SUPPORTED_NAMES.includes(name)) {
      throw new Error("Concorso not known.")
    }
    this.name = name
    this.questions = this.loadQuestions()
  }

  loadQuestions () {
    return require(`~/static/${this.name}.js`);
  }

  getRandomQuestion (skipList = []) {
    let questions = this.questions.filter((item) => {
      return !skipList.includes(item.number)
    })
    return questions[Math.floor(Math.random() * questions.length)];
  }

  getQuestionByNumber (number) {
    return this.questions.find((item) => {
      return parseInt(item.number) === parseInt(number);
    })
  }
}
