import { SUPPORTED_NAMES } from './constant'
export default class Statistiche {

  constructor (name) {
    if (!SUPPORTED_NAMES.includes(name)) {
      throw new Error("Concorso not known.")
    }
    this.name = name
    this.itemKey = this.name
    this.simulazioniItemKey = `simulazioni-${this.name}`
  }

  increment (number, correct) {
    const currentHistory = JSON.parse(window.localStorage.getItem(this.itemKey)) || {};

    if (!currentHistory[number]) {
      currentHistory[number] = {
        answered: 0,
        correct: 0,
        wrong: 0
      }
    }

    currentHistory[number].answered++;
    if (correct) currentHistory[number].correct++;
    else currentHistory[number].wrong++;

    window.localStorage.setItem(this.itemKey, JSON.stringify(currentHistory))
  }

  getHistory () {
    return JSON.parse(window.localStorage.getItem(this.itemKey)) || {}
  }

  getSimulationHistory () {
    return JSON.parse(window.localStorage.getItem(this.simulazioniItemKey)) || []
  }

  getListOfQuestionsNumbersAlreadyAnswered () {
    const history = this.getHistory();
    return Object.keys(history);
  }

  addSimulationResult (risultato) {
    const currentSimulationHistory = JSON.parse(window.localStorage.getItem(this.simulazioniItemKey)) || [];

    currentSimulationHistory.push({
      score: risultato,
      date: new Date()
    })

    window.localStorage.setItem(this.simulazioniItemKey, JSON.stringify(currentSimulationHistory))
  }
}
